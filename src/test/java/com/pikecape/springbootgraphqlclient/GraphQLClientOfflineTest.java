package com.pikecape.springbootgraphqlclient;

import com.pikecape.springbootgraphqlclient.GraphQLClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.pikecape.queryloader.QueryLoader;
import java.util.HashMap;
import java.util.Map;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Rule;

/**
 * GraphQLClientOfflineTest.
 * 
 * @author Mika J. Korpela
 */
public class GraphQLClientOfflineTest
{
    public GraphQLClientOfflineTest()
    {
    }
    
    private final String url = "http://localhost:8080/graphql";
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8080);
    
    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQuery() throws JsonProcessingException
    {
        System.out.println("query");
        
        String response = "{\"data\": {\"ships\": [{\"id\": \"GOMSTREE\"}]}}";
        
        String queryFileName = "src/test/resources/ships_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url);
        JsonNode result = graphQLClient.query(query);
        assertNotNull(result);
        assertEquals(1, result.at("/data/ships").size());
        assertTrue(result.at("/data/ships").isArray());
    }

    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQueryWithVariables() throws JsonProcessingException
    {
        System.out.println("queryWithVariables");
        
        String response = "{\"data\":{\"ship\":{\"id\":\"ELSBETH3\",\"name\":\"Elsbeth III\"}}}";
        
        String queryFileName = "src/test/resources/ship_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        Map<String, Object> variables = new HashMap<>();
        variables.put("id", "ELSBETH3");
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url);
        JsonNode result = graphQLClient.query(query, variables);
        assertNotNull(result);
        assertEquals("Elsbeth III", result.at("/data/ship").get("name").asText());
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQueryBadResponse() throws JsonProcessingException
    {
        System.out.println("query");
        
        String response = "{\"data\": \"ships\": [{\"id\": \"GOMSTREE\"}]}}";
        
        String queryFileName = "src/test/resources/ships_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url);
        JsonNode result = graphQLClient.query(query);
        assertNull(result);
    }

    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQueryWithVariablesBadResponse() throws JsonProcessingException
    {
        System.out.println("queryWithVariables");
        
        String response = "{\"data\":\"ship\":{\"id\":\"ELSBETH3\",\"name\":\"Elsbeth III\"}}}";
        
        String queryFileName = "src/test/resources/ship_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        Map<String, Object> variables = new HashMap<>();
        variables.put("id", "ELSBETH3");
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url);
        JsonNode result = graphQLClient.query(query, variables);
        assertNull(result);
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQueryNullResponse() throws JsonProcessingException
    {
        System.out.println("query");
        
        String response = null;
        
        String queryFileName = "src/test/resources/ships_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url);
        JsonNode result = graphQLClient.query(query);
        assertNull(result);
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQueryWithVariablesNullResponse() throws JsonProcessingException
    {
        System.out.println("queryWithVariables");
        
        String response = null;
        
        String queryFileName = "src/test/resources/ship_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        Map<String, Object> variables = new HashMap<>();
        variables.put("id", "ELSBETH3");
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url);
        JsonNode result = graphQLClient.query(query, variables);
        assertNull(result);
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQueryBadRequest() throws JsonProcessingException
    {
        System.out.println("query");
        
        String response = "{\"data\": {\"ships\": [{\"id\": \"GOMSTREE\"}]}}";
        
        String queryFileName = "src/test/resources/ships_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(400)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url);
        
        try {
            graphQLClient.query(query);
            fail();
        } catch (RuntimeException runtimeException) {
            assertNotNull(runtimeException);
            assertEquals(runtimeException.getMessage(), "400 Bad Request");
        }
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQueryUnexpectedStatusCode() throws JsonProcessingException
    {
        System.out.println("query");
        
        String response = "{\"data\": {\"ships\": [{\"id\": \"GOMSTREE\"}]}}";
        
        String queryFileName = "src/test/resources/ships_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(204)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url);
        JsonNode result = graphQLClient.query(query);
        assertNull(result);
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    @Test
    public void testQueryWithAuthentication() throws JsonProcessingException
    {
        System.out.println("query");
        
        String response = "{\"data\": {\"ships\": [{\"id\": \"GOMSTREE\"}]}}";
        
        String queryFileName = "src/test/resources/ships_query.txt";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        
        stubFor(post("/graphql")
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(response)));
        
        GraphQLClient graphQLClient = new GraphQLClient(url, "Duey", "Duck");
        JsonNode result = graphQLClient.query(query);
        assertNotNull(result);
        assertEquals(1, result.at("/data/ships").size());
        assertTrue(result.at("/data/ships").isArray());
    }
}

