package com.pikecape.springbootgraphqlclient;

import com.pikecape.springbootgraphqlclient.GraphQLClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.pikecape.queryloader.QueryLoader;
import java.util.HashMap;
import java.util.Map;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * GraphQLClientOnlineTest.
 * 
 * @author Mika J. Korpela
 */
public class GraphQLClientOnlineTest
{
    
    public GraphQLClientOnlineTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }

    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testQuery()
    {
        System.out.println("query");
        
        String queryFileName = "src/test/resources/past_launches_query.txt";
        String url = "https://api.spacex.land/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        GraphQLClient instance = new GraphQLClient(url);
        JsonNode result = instance.query(query);
        
        assertNotNull(result);
        assertNotNull(result.get("data"));
        assertEquals(10, result.at("/data/launchesPast").size());
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testQueryWithStringVariables()
    {
        System.out.println("queryWithStringVariables");
        
        String queryFileName = "src/test/resources/ship_query.txt";
        String url = "https://api.spacex.land/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        Map<String, Object> variables = new HashMap<>();
        variables.put("id", "ELSBETH3");
        
        GraphQLClient instance = new GraphQLClient(url);
        
        JsonNode result = instance.query(query, variables);
        assertNotNull(result);
        assertNotNull(result.get("data"));
        assertEquals("Elsbeth III", result.at("/data/ship").get("name").asText());
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testQueryWithIntVariables()
    {
        System.out.println("queryWithIntVariables");
        
        String queryFileName = "src/test/resources/rockets_query.txt";
        String url = "https://api.spacex.land/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        Map<String, Object> variables = new HashMap<>();
        variables.put("limit", 3);
        
        GraphQLClient instance = new GraphQLClient(url);
        
        JsonNode result = instance.query(query, variables);
        assertNotNull(result);
        assertNotNull(result.get("data"));
        assertEquals(3, result.at("/data/rockets").size());
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testQueryMalformedQuery()
    {
        System.out.println("queryMalformedQuery");
        
        String queryFileName = "src/test/resources/malformed_query.txt";
        String url = "https://api.spacex.land/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        GraphQLClient instance = new GraphQLClient(url);
        
        try {
            instance.query(query);
            fail();
        } catch (RuntimeException runtimeException) {
            assertNotNull(runtimeException);
            assertEquals(runtimeException.getMessage(), "400 Bad Request");
        }
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testQueryUnknownQuery()
    {
        System.out.println("queryUnknownQuery");
        
        String queryFileName = "src/test/resources/unknown_query.txt";
        String url = "https://api.spacex.land/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        GraphQLClient instance = new GraphQLClient(url);
        
        try {
            instance.query(query);
            fail();
        } catch (RuntimeException runtimeException) {
            assertNotNull(runtimeException);
            assertEquals(runtimeException.getMessage(), "400 Bad Request");
        }
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testQueryMissingVariables()
    {
        System.out.println("queryMissingVariables");
        
        String queryFileName = "src/test/resources/ship_query.txt";
        String url = "https://api.spacex.land/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        Map<String, Object> variables = new HashMap<>();
        
        GraphQLClient instance = new GraphQLClient(url);
        
        try {
            instance.query(query, variables);
            fail();
        } catch (RuntimeException runtimeException) {
            assertNotNull(runtimeException);
            assertEquals(runtimeException.getMessage(), "400 Bad Request");
        }
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testQueryWrongVariables()
    {
        System.out.println("queryWrongVariables");
        
        String queryFileName = "src/test/resources/ship_query.txt";
        String url = "https://api.spacex.land/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        Map<String, Object> variables = new HashMap<>();
        variables.put("duey", "tupu");
        
        GraphQLClient instance = new GraphQLClient(url);
        
        try {
            instance.query(query, variables);
            fail();
        } catch (RuntimeException runtimeException) {
            assertNotNull(runtimeException);
            assertEquals(runtimeException.getMessage(), "400 Bad Request");
        }
    }
}
