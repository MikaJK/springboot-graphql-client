package com.pikecape.springbootgraphqlclient;

import com.pikecape.springbootgraphqlclient.GraphQLClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.pikecape.queryloader.QueryLoader;
import java.util.HashMap;
import java.util.Map;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * GraphQLClientLocalServerTest.
 * 
 * @author Mika J. Korpela
 */
public class GraphQLClientLocalServerTest
{
    
    public GraphQLClientLocalServerTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }

    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testMutation()
    {
        System.out.println("mutation");
        
        String queryFileName = "src/test/resources/create_vehicle_mutation.txt";
        String url = "http://localhost:8080/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        GraphQLClient instance = new GraphQLClient(url);
        JsonNode result = instance.query(query);
        
        assertNotNull(result);
        System.out.println("MUTATION RESULT: " + result);
    }
    
    /**
     * Test of query method, of class GraphQLClient.
     */
    @Test
    public void testQuery()
    {
        System.out.println("query");
        
        String queryFileName = "src/test/resources/query_vehicle_query.txt";
        String url = "http://localhost:8080/graphql/";
        QueryLoader queryLoader = new QueryLoader();
        String query = queryLoader.load(queryFileName);
        GraphQLClient instance = new GraphQLClient(url);
        JsonNode result = instance.query(query);
        
        assertNotNull(result);
        System.out.println("QUERY RESULT: " + result);
    }
}
