package com.pikecape.queryloader;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * QueryLoaderTest.
 * 
 * @author Mika J. Korpela
 */
public class QueryLoaderTest
{
    public QueryLoaderTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }

    /**
     * Test of load method, of class QueryLoader.
     */
    @Test
    public void testLoad_OK()
    {
        System.out.println("load_OK");
        
        String fileName = "src/test/resources/test_query.txt";
        QueryLoader instance = new QueryLoader();
        
        int numberOfCharacters = 545;
        int numberOfBytes = 545;
        
        String result = instance.load(fileName);
        assertNotNull(result);
        assertEquals(numberOfCharacters, result.length());
        assertEquals(numberOfBytes, result.getBytes().length);
    }
    
    /**
     * Test of load method, of class QueryLoader.
     */
    @Test
    public void testLoad_NOK_NonExistingFile()
    {
        System.out.println("load_NOK_NonExistingFile");
        
        String fileName = "src/test/resources/test_non_existing.txt";
        QueryLoader instance = new QueryLoader();
        
        String result = instance.load(fileName);
        assertNull(result);
    }
}
