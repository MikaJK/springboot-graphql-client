package com.pikecape.springbootgraphqlclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * GraphQLClient.
 *
 * @author Mika J. Korpela
 */
public class GraphQLClient
{
    // attributes.
    private final String url;
    private String username;
    private String password;
    private final HttpHeaders requestHeader;
    private String authenticationHeader;

    // logger.
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphQLClient.class);

    /**
     * Constructor.
     *
     * @param url
     */
    public GraphQLClient(String url)
    {
        this.url = url;
        this.requestHeader = new HttpHeaders();
    }

    /**
     * Constructor with authentication header.
     *
     * @param url
     * @param username
     * @param password
     */
    public GraphQLClient(String url, String username, String password)
    {
        this.url = url;
        this.username = username;
        this.password = password;
        this.requestHeader = new HttpHeaders();
        this.setAuthenticationHeader();
    }

    /**
     * Set authentication header.
     *
     */
    private void setAuthenticationHeader()
    {
        String credentials = this.username + ":" + this.password;
        byte[] encodedCredentials = Base64.encodeBase64(credentials.getBytes());
        this.authenticationHeader = "Basic " + new String(encodedCredentials);
        this.requestHeader.add("Authorization", this.authenticationHeader);
    }

    /**
     * Query; without variables.
     *
     * @param query
     * @return JsonNode
     */
    public JsonNode query(String query)
    {
        // variables.
        GraphQLQuery graphqlQuery = new GraphQLQuery();
        String response;

        try {
            // set query.
            graphqlQuery.setQuery(query);

            // call graphql service.
            response = this.httpPost(this.url, new HashMap<>(), graphqlQuery, String.class);
            
            // check response.
            if (response == null) {
                // write log entries and return null.
                LOGGER.error("GraphQL query failed or did not produced any results.");
                return null;
            }

            // return response as json node.
            return new ObjectMapper().readTree(response);
        } catch (JsonProcessingException jsonProcessingException) {
            // write log entries and return null.
            LOGGER.error("Processing query results failed.");
            LOGGER.error(jsonProcessingException.getMessage());
            return null;
        }
    }

    /**
     * Query; with variables.
     *
     * @param query
     * @param variables
     * @return JsonNode
     */
    public JsonNode query(String query, Map<String, Object> variables)
    {
        // variables.
        GraphQLQuery graphqlQuery = new GraphQLQuery();
        String response;

        try {
            // set query and variables.
            graphqlQuery.setQuery(query);
            graphqlQuery.getVariables().putAll(variables);

            // call graphql service.
            response = this.httpPost(this.url, new HashMap<>(), graphqlQuery, String.class);
            
            // check response.
            if (response == null) {
                // write log entries and return null.
                LOGGER.error("GraphQL query failed or did not produced any results.");
                return null;
            }

            // return response as json node.
            return new ObjectMapper().readTree(response);
        } catch (JsonProcessingException jsonProcessingException) {
            // write log entries and return null.
            LOGGER.error("Processing query results failed.");
            LOGGER.error(jsonProcessingException.getMessage());
            return null;
        }
    }

    /**
     * HTTP post.
     *
     * @param <T>
     * @param <R>
     * @param url
     * @param parameters
     * @param body
     * @param responseClass
     * @return R
     */
    public <T, R> R httpPost(String url, Map<String, String> parameters, T body, Class<R> responseClass)
    {
        // variables.
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<R> responseEntity;

        // initialize rest template with error handler.
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler());

        // set header,
        this.requestHeader.setContentType(MediaType.APPLICATION_JSON);
        this.requestHeader.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        try {
            // call rest service.
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(body, this.requestHeader), responseClass, parameters);

            // http status is either ok or created.
            if (responseEntity.getStatusCode().equals(HttpStatus.OK) || responseEntity.getStatusCode().equals(HttpStatus.CREATED)) {
                // return body.
                return responseEntity.getBody();
            } else {
                // write log entries.
                LOGGER.error("Unexpected status code {}. Expected 200 or 201.", responseEntity.getStatusCode());
                
                // return null.
                return null;
            }
        } catch (RestClientException restClientException) {

            // write log entries.
            LOGGER.error("HTTP post to URL {} failed.", url);
            LOGGER.error(restClientException.getMessage());

            // throw run-time exception.
            throw new RuntimeException(restClientException.getMessage());
        }
    }
}
