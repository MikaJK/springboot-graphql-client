package com.pikecape.springbootgraphqlclient;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;

/**
 * GraphQLQuery.
 * 
 * @author Mika J. Korpela
 */
public class GraphQLQuery
{
    // attributes.
    @JsonProperty("query")
    private String query;
    @JsonProperty("variables")
    private final Map<String, Object> variables = new HashMap<>();

    // getters and setters.
    public Map<String, Object> getVariables()
    {
        return variables;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }
}