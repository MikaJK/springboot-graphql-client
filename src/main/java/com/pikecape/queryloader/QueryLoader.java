package com.pikecape.queryloader;

import com.pikecape.springbootgraphqlclient.GraphQLClient;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * QueryLoader.
 *
 * @author Mika J. Korpela
 */
public class QueryLoader
{
    // logger.
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphQLClient.class);

    /**
     * Load query from text file to string.
     *
     * @param fileName
     * @return String
     */
    public String load(String fileName)
    {
        // initialize query string builder.
        StringBuilder queryStringBuilder = new StringBuilder();

        try {
            // read all lines from text file.
            Stream<String> queryStream = Files.lines(Paths.get(fileName), StandardCharsets.UTF_8);

            // append all lines to query string builder.
            queryStream.forEach(s -> queryStringBuilder.append(s).append("\n"));
        } catch (IOException ioException) {
            // write log entries.
            LOGGER.error("Reading file {} failed.", fileName);
            LOGGER.error(ioException.getMessage());

            // return null.
            return null;
        }

        // return query string builder content as string.
        return queryStringBuilder.toString();
    }
}
